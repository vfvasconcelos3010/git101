//
//  Git101TopApp.swift
//  Git101Top
//
//  Created by Victor Vasconcelos on 14/05/24.
//

import SwiftUI

@main
struct Git101TopApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
