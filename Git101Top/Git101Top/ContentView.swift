//
//  ContentView.swift
//  Git101Top
//
//  Created by Victor Vasconcelos on 14/05/24.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "gamecontroller.fill")
                .imageScale(.large)
                .foregroundStyle(.tint)
            Text("Hello, Amazônia!")
                .foregroundStyle(.blue)
                .font(.largeTitle)
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
